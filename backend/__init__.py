from flask import Flask
from flask_misaka import Misaka
import os

app = Flask(__name__)
Misaka(app)
app.secret_key = os.urandom(24)

from . import routes
from . import models
