from flask import render_template, session, redirect, request, url_for, flash
import functools
from . import app
import re
from .models import Uzivatel
from pony.orm import db_session

############################################################################

slova = ("Super", "Perfekt", "Úža", "Flask")


def prihlasit(function):
    @functools.wraps(function)
    def wrapper(*args, **kwargs):
        if "user" in session:
            return function(*args, **kwargs)
        else:
            return redirect(url_for("login", url=request.path))

    return wrapper


############################################################################


@app.route("/index/")
@app.route("/")
def index():
    return render_template("base.html.j2", slova=slova)


@app.route("/saveemail/", methods=["POST"])
@db_session
def saveemail():
    email = request.form.get("email")
    password = "password"
    name = "name"
    re_email = re.compile(
        r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)"
    )
    if re_email.search(email):
        u = Uzivatel.get(email=email)
        if u:
            flash("Zdá se, že tato adresa již v databázi je.")
        else:
            Uzivatel(email=email, password=password, name=name)
            flash("Uloženo")
    else:
        flash("Zdá se, že toto není emailová adresa...")
    return redirect(url_for("index", email=email))
